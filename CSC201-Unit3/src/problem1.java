
public class problem1 

/**
 * Algorithm to solve the problem
 * 1) create the variables for wall floor and windows
 * 2) create a constructor for each variable
 * 3) create getters and setters for the variables
 * 4) create a toString method
 * 5) create a demo class where the predefined characteristics are put
 */
{
	
	private String wall;
	private String floor;
	private int windows; 

	
	public problem1()
	{
		
	
		
		wall="";
		floor="";
		windows = getWindows();
		
	}

	public int getWindows() 
	{
		return windows;
	}

	public void setWindows(int windows)
	{
		this.windows = windows;
	}

	public problem1(String color)
	{
		this.wall = color; 
	}

	public problem1(String wallColor, String floorType, int numberOfWindows) 
	{
		wall = wallColor; 
		floor = floorType;
		windows = numberOfWindows;
				
	}

	public String toString()
	{
		return this.wall + "\n" + this.floor + "\n" + this.windows;
		
		
	}
	public void setWall(String color)
	{
		this.wall = color; 
	}

	public String getWall()
	{
		return this.wall;
	}

	public String getFloor() 
	{
		return floor;
	}

	public void setFloor(String floor)
	{
		this.floor = floor;
	}

}
