
public class problem1Demo {
	/**
	 * Contains the predefined characteristics of the walls, floors and windows
	 * @param args
	 */

	public static void main(String[] args)
	{
	
		problem1 aRoom = new problem1("Yellow","Hardwood",1 );
		System.out.println("Room 1");
		System.out.println(aRoom);
		
		problem1 aRoom2 = new problem1("Purple","Tiled Floors", 0);
		System.out.println("Room 2");
		System.out.println(aRoom2);
		
		problem1 aRoom3 = new problem1("White", "Carpented", 3);
		System.out.println("Room 3");
		System.out.println(aRoom3);
		
	}
}
