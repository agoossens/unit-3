
public class problem3 
{
	/**
	 * Algorithm to solve the problem
	 * 1) Create the different variables to define wheels and weight
	 * 2) Create a predefined constructor 
	 * 3) Assign the values to wheels and weight, 100 & 1000
	 * 4) create a to string method
	 * 5) create a demo class to print out the beginning and changed result
	 */
	private int numberOfWheels;
	private int weight;
	


	public problem3()
	{
		numberOfWheels = 100;
		weight = 1000;
	}
	
	public problem3 (int numberOfWheels, int weight)
	{
		this.numberOfWheels = numberOfWheels;
		this.weight = weight; 
	}

	public String toString()
	{
		return (""+numberOfWheels+","+weight);
	}
}
