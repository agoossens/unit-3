import java.util.Scanner;


public class problem6 {
	/**
	 * Algorithm to solve the problem
	 * 1) Create a method CharacterArray
	 * 2) Allow the user to enter a string
	 * 3) store the string in a char array
	 * 4) create a for loop which is the length of the array
	 * 5) go through the array, first make the lower case upper case and upper case lower case
	 * 6) make the blanks stay blanks
	 * 7) use the else statement to make the remainder (numbers) to *
	 * 8) print out the final result
	 * @param args
	 */
			
	public static void main(String[] args)
	{
		CharacterArray();
	}
	public static  void CharacterArray()
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter a string.");
		String convertString = keyboard.nextLine(); //stores the string as convertString

		String star = "*";
		char c = star.charAt(0); //stores the char c as *

		char changeString[] = convertString.toCharArray();


		for(int i=0;i<changeString.length; i++) //runs the for loop as long as the length of the array
		{
			if(Character.isLowerCase(changeString[i])) //if the array[i] is lower case
			{
				System.out.println(changeString[i]);
				changeString[i] = Character.toUpperCase(changeString[i]);
				System.out.println(changeString[i]);
			}
			else if(Character.isUpperCase(changeString[i])) //if the array[i] is upper case 
			{
				System.out.println(changeString[i]);
				changeString[i] = Character.toLowerCase(changeString[i]);
				System.out.println(changeString[i]);
			}
			else if (changeString[i] == ' ') //if the array[i] is a space
			{
				System.out.println(changeString[i]);
				changeString[i] = changeString[i];
				System.out.println(changeString[i]);
			}

			else  //the final thing that is left is a number
			{ 
				System.out.println(changeString[i]);
				changeString[i] = c; 
				System.out.println(changeString[i]);
			}
		}
		System.out.println(changeString);


	}


}