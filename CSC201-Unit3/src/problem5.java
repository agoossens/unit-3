import java.util.Scanner;


public class problem5 {
	/**
	 * Algoritm to solve the problem
	 * 1) create a separate method called stringBuilder
	 * 2) use the sb to enter the different parts of the string
	 * 3) use append, replace and insert to put the different strings on the correct part
	 * 4) print out the final result
	 * @param args
	 */
	public static void main(String[] args)
	{
	stringBuilder();
		

	}
	public static void stringBuilder()
	{		
		StringBuilder sb = new StringBuilder(); //Creates the string builder
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the string: Java is fun!"); //allows the user to enter the different strings
		String firstPart = keyboard.nextLine(); //creates the different sentences
		System.out.println("Please enter the string: I love it!");
		String secondPart = keyboard.nextLine();
		System.out.println("Please enter the string: Yes,");
		String thirdPart = keyboard.nextLine();
		
		sb.append(firstPart); //puts the first sentence into the sb
		sb.append(" " + secondPart); //adds the first string with the second string to the sb
		
		sb.replace(11, 12, ".");	//gets rid of the ! and replaces it with . 
		sb.insert(13, thirdPart + " "); // adds the third part in between the first and second part 
		
		System.out.println(sb); //prints out the final result
		
	}
}
